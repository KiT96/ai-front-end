import { BrowserModule, Title } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { NgxPaginationModule } from 'ngx-pagination';
import { MatSliderModule } from '@angular/material/slider';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatListModule } from '@angular/material/list';
import { MatLineModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MatRadioModule } from '@angular/material/radio';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { CookieService } from "ngx-cookie-service";


import { AppComponent } from "./app.component";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./auth/login/login.component";
import { RegisterComponent } from "./auth/register/register.component";
import { NavMenuComponent } from "./nav-menu/nav-menu.component";
import { ErrorValidateComponent } from "./auth/error-validate/error-validate.component";
import { TrainingComponent } from "./training/training.component";
import { DetectComponent } from "./detect/detect.component";
import { ToolComponent } from "./tool/tool.component";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UploadCustomComponent } from "./upload-custom/upload-custom.component";
import { OcrListComponent } from "./ocr-list/ocr-list.component";
import { YoloListComponent } from "./yolo-list/yolo-list.component";
import { SiameseListComponent } from "./siamese-list/siamese-list.component";
import { UploadMultipleComponent } from "./upload-multiple/upload-multiple.component";
import { SampleListComponent } from "./sample-list/sample-list.component";
import { DialogImageComponent } from "./dialog-image/dialog-image.component";
import { DetectListImageComponent } from "./detect-list-image/detect-list-image.component";

import { NotFoundComponent } from "./shared/not-found/not-found.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    NavMenuComponent,
    ErrorValidateComponent,
    TrainingComponent,
    DetectComponent,
    ToolComponent,
    UploadCustomComponent,
    OcrListComponent,
    YoloListComponent,
    SiameseListComponent,
    UploadMultipleComponent,
    SampleListComponent,
    DialogImageComponent,
    DetectListImageComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "ng-cli-universal" }),
    HttpClientModule,
    NgxPaginationModule,
    ReactiveFormsModule,
    FormsModule,
    MatSliderModule,
    MatTableModule,
    MatInputModule,
    MatPaginatorModule,
    MatListModule,
    MatLineModule,
    MatIconModule,
    MatRadioModule,
    MatFormFieldModule,
    MatDialogModule,
    MatProgressBarModule,
    RouterModule.forRoot([
      { path: '', component: TrainingComponent, pathMatch: "full" },
      { path: 'training', component: TrainingComponent },
      { path: 'detect', component: DetectComponent },
      { path: 'detect-list', component: DetectListImageComponent },
      { path: 'tool', component: ToolComponent },
      { path: 'upload-custom', component: UploadCustomComponent },
      { path: 'upload-multiple', component: UploadMultipleComponent },
      { path: 'home/auth/login', component: LoginComponent },
      { path: 'home/auth/register', component: RegisterComponent },
      { path: '404', component: NotFoundComponent}, 
    ]),
    BrowserAnimationsModule
  ],
  providers: [Title, CookieService],
  bootstrap: [AppComponent],
  entryComponents: [DialogImageComponent, SampleListComponent]
})
export class AppModule { }
