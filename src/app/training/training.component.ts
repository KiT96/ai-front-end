import { Component, OnInit } from '@angular/core';
import { TrainingService } from "../models/services/training.service";
import { AccountService } from '../models//services/account.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SampleListComponent } from '../sample-list/sample-list.component';
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {
  public trainingService: TrainingService = null;
  public accountService: AccountService = null;
  public data: any;
  public epochs = 50;
  public models = '';
  public router: Router;
  public page: number = 1;
  public title: string = 'Home';

  constructor(private titleService: Title, _trainingService: TrainingService, _accountService: AccountService, _router: Router, private dialog: MatDialog) {
    this.trainingService = _trainingService;
    this.accountService = _accountService;
    this.router = _router;
  }

  ngOnInit() {
    this.titleService.setTitle(this.title)
    this.getResource();
  }

  public getResource() {
    this.trainingService.getResource(res => {
      this.data = res;
    }, fail => {
      if (fail.status == 401) {
        this.router.navigate(['/home/auth/login']);
      }
    })
  }

  public onTraining() {
    this.trainingService.training({ 'model': this.models, 'epochs': this.epochs }, res => { }, fail => { });
  }

  getSamples = (id: any) => {
    const sampleList = this.data.versions.filter((version: { id: any; }) => version.id == id)
    const dialogConfig = new MatDialogConfig();
    dialogConfig.height = '80%';
    dialogConfig.width = '85%'
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      data: sampleList
    }
    this.dialog.open(SampleListComponent, dialogConfig);
  }

}
