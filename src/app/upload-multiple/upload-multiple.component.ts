import { Component, OnInit } from '@angular/core';
import { AccountService } from '../models/services/account.service';
import { UploadService } from '../models/services/upload.service';
import { HttpEventType } from '@angular/common/http';
import { Router } from "@angular/router";
import { CookieService } from 'ngx-cookie-service';
import { environment } from "../../environments/environment";

@Component({
  selector: 'app-upload-multiple',
  templateUrl: './upload-multiple.component.html',
  styleUrls: ['./upload-multiple.component.css']
})
export class UploadMultipleComponent implements OnInit {
  public fileDatas: any;
  public accountService: AccountService = null;
  public uploadService: UploadService = null;
  public formData: FormData = null;
  public router: Router;

  constructor(_accountService: AccountService, _uploadService: UploadService, _router: Router, private cookieService: CookieService) { 
    this.accountService = _accountService; 
    this.uploadService = _uploadService; 
    this.router = _router 
  }

  ngOnInit() {
   
  }

  getFormData = (files: string | any[]) => {
    this.fileDatas = files;

    this.formData = new FormData();
    for (let index = 0; index < this.fileDatas.length; index++) {
      const element = this.fileDatas[index];
      this.formData.append('files', element, element.name);
    }
  }

  upload() {
    const headers = { 'Authorization': 'Token ' + this.accountService.getCookies('token') };
    this.uploadService.http.post(environment.root_url + 'images/image-upload/', this.formData, { headers, reportProgress: true, observe: 'events' })
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          if(event.status == 200){
            this.router.navigate(['/tool'])
          }
        }
      }, fail => { })
  }

}
