import { Component, OnInit, Input } from '@angular/core';
import { TrainingService } from '../models//services/training.service';

@Component({
  selector: 'app-ocr-list',
  templateUrl: './ocr-list.component.html',
  styleUrls: ['./ocr-list.component.css']
})
export class OcrListComponent implements OnInit {

  public trainingService: TrainingService = null;
  public data: any;
  public page: number = 1;

  public displayedColumns: string[] = ['id', 'filename', 'created', 'using','action'];

  constructor(_trainingService: TrainingService) { this.trainingService = _trainingService }

  ngOnInit() {
    this.getOcrList();
  }

  public getOcrList(){
    this.trainingService.getResource(res => {this.data = res;
     }, fail => console.log('fail: ', fail))
  }

  uploadImages = (event) => {
    console.log('ocr: ', event);
    
  }

}
