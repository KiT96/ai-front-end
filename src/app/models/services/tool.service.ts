import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { AccountService } from './account.service';
import { CookieService } from "ngx-cookie-service";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ToolService {
  public http: HttpClient = null;
  public accountService: AccountService = null;

  constructor(_http: HttpClient, _accountService: AccountService, private cookieService: CookieService) {
    this.http = _http;
    this.accountService = _accountService
  }

  fetchImages(success: (arg0: Object) => void, failed: (arg0: any) => void) {
    const headers = { 'Authorization': 'Token ' + this.accountService.getCookies('token') };
    this.http.get(environment.root_url + "fetch/images/", { headers }).subscribe(res => success(res), fail => failed(fail))
  }

  addLabel = (data: any, success: (arg0: Object) => void, failed: (arg0: any) => void) => {
    const headers = { 'Authorization': 'Token ' + this.accountService.getCookies('token') };
    this.http.post(environment.root_url + "images/label/", data, { headers }).subscribe(res => { success(res); console.log('success: ', res) }, fail => { failed(fail); console.log('fail: ', fail) })
  }
}
