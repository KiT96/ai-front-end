import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType } from "@angular/common/http";
import { AccountService } from './account.service';
import { CookieService } from "ngx-cookie-service";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  public http: HttpClient = null;
  public accountService: AccountService = null;

  constructor(_http: HttpClient, _accountService: AccountService, private cookieService: CookieService) { 
    this.http = _http; 
    this.accountService = _accountService 
  }

  upload = (formData: any, success: (arg0: Object) => void, failed: (arg0: any) => void) => {
    const headers = { 'Authorization': 'Token ' + this.accountService.getCookies('token') };
    this.http.post(environment.root_url + 'detection/yolo/predict/', formData, {headers, reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)  Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          success(event.body)
        }
      }, fail => failed(fail))
  }
}
