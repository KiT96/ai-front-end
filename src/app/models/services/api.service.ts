import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { AccountService } from './account.service';
import { DetectService } from './detect.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public http: HttpClient = null;
  public accountService: AccountService = null;
  public detectService: DetectService = null;

  constructor(_http: HttpClient, _accountService: AccountService, _detectService: DetectService) {
    this.http = _http;
    this.accountService = _accountService;
    this.detectService = _detectService;
  }
}
