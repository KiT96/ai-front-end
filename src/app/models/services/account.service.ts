import { Injectable, Inject } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Account } from '../Account';
import { BehaviorSubject, Observable } from 'rxjs';
import { CookieService } from "ngx-cookie-service";
import { Router } from '@angular/router';
import { environment } from "../../../environments/environment";

function setCookie(name: string,value: any,days: number) {
  var expires = "";
  if (days) {
      var date = new Date();
      date.setTime(date.getTime() + (days*24*60*60*1000));
      expires = "; expires=" + date.toUTCString();
  }
  document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name: string) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
  }
  return null;
}

function eraseCookie(name: string) {   
  document.cookie = name+'=; Max-Age=-99999999;';  
}

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  public http: HttpClient = null;
  public baseUrl: string = '127.0.0.1:8000';
  public userInfo: any;

  // BehaviorSubject 
  public onLogined: BehaviorSubject<any> = new BehaviorSubject('');
  onLogin$: Observable<any> = this.onLogined.asObservable();

  constructor(_http: HttpClient, private cookieService: CookieService, private router: Router) {
    this.http = _http;
  }

  setCookies(name: string,value: any,days: number){
    setCookie(name, value, days);
  }

  getCookies(name: string){
    return getCookie(name);
  }

  eraseCookies(name: string){
    eraseCookie(name);
  }

  login(account: Account, success: any, failed: any) {
    this.http.post(environment.root_url + 'account/login', account)
      .subscribe(res => { 
        this.onLogined.next(res);
        success(res); 
        this.userInfo = res; 
        this.setCookies('token', this.userInfo.token, 3)
        this.setCookies('email', this.userInfo.email, 3)
        this.setCookies('is_superuser', this.userInfo.is_superuser, 3)
      }, err => failed(err));
  }

  register(account: Account, success: any, failed: any) {
    this.http.post(this.baseUrl + 'api/account/register', account)
      .subscribe(res => success(res), err => failed(err));
  }

  logout = () => {
    const headers = { 'Authorization': 'Token ' + this.getCookies('token') };
    this.http.delete(environment.root_url + 'account/logout', { headers }).subscribe(() => {
      this.eraseCookies('token');
      this.eraseCookies('email')
      this.eraseCookies('is_superuser')
      this.router.navigate(['/home/auth/login']);
    }, fail => {})
  }

 
}
