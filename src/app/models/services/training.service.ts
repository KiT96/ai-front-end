import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpEventType } from "@angular/common/http";
import { AccountService } from './account.service';
import { CookieService } from "ngx-cookie-service";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TrainingService {
  public http: HttpClient = null;
  public accountService: AccountService = null;

  constructor(_http: HttpClient, _accountService: AccountService, private cookieService: CookieService) {
    this.http = _http;
    this.accountService = _accountService;
  }

  getResource(success: (arg0: Object) => void, failed: (arg0: any) => void) {
    if (this.accountService.getCookies('token') != null) {
      const headers = { 'Authorization': 'Token ' + this.accountService.getCookies('token') };
      this.http.get(environment.root_url + 'detection/version/', { headers }).subscribe(res => {success(res);
      }, fail => failed(fail))
    }else{
      failed('failed')
    }
  }

  training = (params: { model: any; epochs: any; }, success: (arg0: Object) => void, failed: (arg0: any) => void) => {
    if (this.accountService.getCookies('token') != null) {
      const headers = { 'Authorization': 'Token ' + this.accountService.getCookies('token') };
      this.http.post(environment.root_url + 'trainer/training', { 'model': params.model, 'epochs': params.epochs }, { headers }).subscribe(res => success(res), fail => failed(fail))
    }else{
      failed('failed')
    }
  }

  download = (id: string, success: (arg0: Object) => void, failed: (arg0: any) => void) => {
    if(this.accountService.getCookies('token') != null){
      const headers = { 'Authorization': 'Token ' + this.accountService.getCookies('token') };
      let params = new HttpParams();
      params = params.append('id', id);
      this.http.get(environment.root_url + 'detection/download', { headers, observe: "body", params }).subscribe(res => success(res), fail => failed(fail))
    }else{
      failed('failed')
    }
  }

  getSamples = (success: (arg0: Object) => void, failed: (arg0: any) => void) => {
    if(this.accountService.getCookies('token') != null){
      const headers = { 'Authorization': 'Token ' + this.accountService.getCookies('token') };
      this.http.get(environment.root_url + 'detection/samples/',{headers}).subscribe( res => success(res), fail => failed(fail));
    }else{
      failed('failed')
    }
  }

  uploadImageSample = (files: string | any[]) => {
    const formData = new FormData();

    for (let index = 0; index < files.length; index++) {
      const element = files[index];
      let fileToUpload = <File>element;

      formData.append('files', fileToUpload, fileToUpload.name);
    }

    const headers = { 'Authorization': 'Token ' + this.accountService.getCookies('token') };
    this.http.post(environment.root_url + 'images/image-upload/', formData, { headers, reportProgress: true, observe: 'events' })
      .subscribe(event => {
        console.log('step 4');
        if (event.type === HttpEventType.UploadProgress) Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          console.log('Response: ', event);
        }
      }, fail => { })
  }

}
``