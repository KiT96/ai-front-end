export class Shape {
    id: any;
    x0: any;
    y0: any;
    x1: any;
    y1: any;
    originX = null;
    originY = null;
    borderColor: any;
    note = '';

    constructor(borderColor: any) {
        this.borderColor = borderColor ? borderColor : '';
    }

    isEnable() {
        return this.x0 && this.y0 && this.x1 && this.y1;
    }

    convertX() {
        if (this.x0 > this.x1) {
            let x = this.x0;
            this.x0 = this.x1;
            this.x1 = x;
        }
    }

    convertY() {
        if (this.y0 > this.y1) {
            let y = this.y0;
            this.y0 = this.y1;
            this.y1 = y;
        }
    }

    draw(ctx: { lineWidth: number; strokeStyle: any; strokeRect: (arg0: any, arg1: any, arg2: number, arg3: number) => void; }) {
        ctx.lineWidth = 3;
        ctx.strokeStyle = this.borderColor;
        ctx.strokeRect(this.x0, this.y0, this.x1 - this.x0, this.y1 - this.y0);
    }

    isHover(pointer: { x: number; y: number; }) {
        if (pointer.x > this.x0 && pointer.x < this.x0 + (this.x1 - this.x0) && pointer.y > this.y0 && pointer.y < this.y0 + (this.y1 - this.y0)) {
            return true;
        }
        return false;
    }
}