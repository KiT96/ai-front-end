export class Account {
    id: number;
    username: string;
    password: string;
    rememberme: boolean;

    constructor(_username: string, _password: string, _rememberme: boolean){
        this.username = _username;
        this.password = _password;
        this.rememberme = _rememberme;
    }
}