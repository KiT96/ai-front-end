import { Component, OnInit } from '@angular/core';
import { UploadService } from '../models/services/upload.service';
import { AccountService } from '../models/services/account.service';
import { DetectService } from '../models/services/detect.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogImageComponent } from '../dialog-image/dialog-image.component';
import { Router } from "@angular/router";

@Component({
  selector: 'app-detect',
  templateUrl: './detect.component.html',
  styleUrls: ['./detect.component.css']
})
export class DetectComponent implements OnInit {
  public uploadService: UploadService = null;
  public accountService: AccountService = null;
  public detectService: DetectService = null;
  public data: any;
  public imagePath: any;
  public progress = 0;
  public history: any;
  public filterHistory: any;
  public page: number = 1;
  public router: Router;

  constructor(_uploadService: UploadService, _accountService: AccountService, _detectService: DetectService, private dialog: MatDialog, _router: Router) {
    this.uploadService = _uploadService;
    this.accountService = _accountService;
    this.detectService = _detectService;
    this.router = _router;
  }

  ngOnInit() {
    if (this.accountService.getCookies('token') != null) {
      this.getHistory()
    } else {
      alert('Vui lòng đăng nhập.')
      this.router.navigate(['/home/auth/login']);
    }
  }

  search = (event: { target: { value: string; }; }) => {
    var result = this.history.images.filter((his: { code: string; }) => (
      his.code.toLowerCase().indexOf(event.target.value) != -1
    ))
    this.filterHistory = { images: result }
  }

  getHistory = () => {
    this.detectService.getHistory(res => {
      this.history = res;
      this.filterHistory = res;
    }, fail => { console.log('fail: ', fail);
      if(fail.status == 401){
        this.router.navigate(['/home/auth/login']);
      }
    });
  }

  getImgUrl = (files: Blob) => {
    var reader = new FileReader();
    reader.readAsDataURL(files);
    reader.onload = (_event) => {
      this.imagePath = reader.result;
    }
  }

  uploadFile = (files: string | any[]) => {
    if (files.length === 0) {
      return;
    } else {
      this.router.navigate(['/detect-list']);
    }
  }

  openImg = (item: any) => {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.height = '570px';
    dialogConfig.width = '400px'
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      srcImg: item.link
    }
    this.dialog.open(DialogImageComponent, dialogConfig);
  }

}
