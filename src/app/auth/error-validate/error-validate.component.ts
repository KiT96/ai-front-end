import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-error-validate',
  templateUrl: './error-validate.component.html',
  styleUrls: ['./error-validate.component.css']
})
export class ErrorValidateComponent implements OnInit {

  @Input('control') control: any;

  constructor() { }

  ngOnInit() {
  }

  get message() {
    for (let err in this.control.errors) {
      if (this.control.dirty) {
        return this.getErrorMessage(err, this.control.errors[err]);
      }
    }
    return null;
  }

  getErrorMessage(err: string, value: { requiredLength: any; }) {
    let message = {
      'pattern': 'Email address is not valid',
      'minlength': `Minlength : ${value.requiredLength} character`,
      'maxlength': `Maxlength : ${value.requiredLength} character`,
    }
    return message[err]
  }

}
