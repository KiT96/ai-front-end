import { Component, OnInit } from '@angular/core';
import { AccountService } from "../../models/services/account.service";
import {TrainingService} from '../../models/services/training.service'
import { Account } from '../../models/Account';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Title } from "@angular/platform-browser";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public title: string = 'EndGam - Log into the website';
  public accountService: AccountService;
  public trainingService: TrainingService = null;
  public formUser: FormGroup;
  public router: Router;

  constructor(_accountService: AccountService, _trainingService: TrainingService, private _formBuider: FormBuilder, private titleService: Title, _router: Router) {
    this.accountService = _accountService;
    this.trainingService = _trainingService;
    this.router = _router;
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title)
    this.createForm();
  }

  createForm() {
    this.formUser = this._formBuider.group({
      username: ['admin', [
        Validators.required,
      ]],
      password: ['Amit123456', [
        Validators.required, Validators.minLength(6)
      ]],
      rememberme: [false]
    })

    this.formUser.valueChanges.subscribe(data => {})
  }

  onSubmit() {
    var value = this.formUser.value;
    var account = new Account(value.username, value.password, value.rememberme);
    this.accountService.login(account, (success: any) => {
      return this.router.navigate(['/training']);
    }, (fail: any) => console.log('Login failed: ', fail));
  }

}
