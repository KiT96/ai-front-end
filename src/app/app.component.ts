import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { AccountService } from './models/services/account.service';
import { Router } from '@angular/router';
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public title: string = 'Home';
  public accountService: AccountService = null;
  public router: Router;

  public constructor(private titleService: Title, _accountService: AccountService, _router: Router, private cookieService: CookieService) {this.accountService = _accountService;this.router = _router;}

  ngOnInit(): void {
    if (this.accountService.getCookies('token') != null) {
      return;
    } else {
      this.router.navigate(['/home/auth/login']);
    }
    this.titleService.setTitle(this.title)
  }
}
