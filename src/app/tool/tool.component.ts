import { Component, OnInit } from '@angular/core';
import { ToolService } from '../models//services/tool.service';
import { Router } from "@angular/router";
import { AccountService } from '../models/services/account.service';

@Component({
  selector: 'app-tool',
  templateUrl: './tool.component.html',
  styleUrls: ['./tool.component.css']
})
export class ToolComponent implements OnInit {

  public toolService: ToolService = null;
  public images: any = [];
  public router: Router;
  public page: number = 1;

  displayedColumns: string[] = ['id', 'file', 'upload_at', 'label'];

  constructor(_toolService: ToolService, _router: Router, private accountService: AccountService) {
    this.toolService = _toolService;
    this.router = _router;
  }

  ngOnInit() {
    if (this.accountService.getCookies('token') != null) {
      this.getImages();
    } else {
      alert('Vui lòng đăng nhập.')
      this.router.navigate(['/home/auth/login']);
    }

  }

  getImages() {
    this.toolService.fetchImages(res => {
      this.images = res;
    }, fail => {
      if (fail.status == 401) {
        this.router.navigate(['/home/auth/login']);
      }
    });
  }

  customFile(event: any) {
    this.router.navigate(['/upload-custom'], { queryParams: { id: event.id, file: event.file, 'upload_at': event.upload_at } });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    console.log('value: ', filterValue);

    this.images.filter = filterValue.trim().toLowerCase();
  }
}

