import { Component, OnInit, Inject } from '@angular/core';
import { TrainingService } from '../models//services/training.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AccountService } from '../models/services/account.service';
import { UploadService } from '../models/services/upload.service';
import { environment } from "../../environments/environment";

@Component({
  selector: 'app-sample-list',
  templateUrl: './sample-list.component.html',
  styleUrls: ['./sample-list.component.css']
})
export class SampleListComponent implements OnInit {

  public trainingService: TrainingService = null;
  public accountService: AccountService = null;
  public uploadService: UploadService = null;
  public data: any;
  public page: number = 1;
  public showAddSample: boolean = true;

  public displayedColumns: string[] = ['name', 'description','action'];


  constructor(_trainingService: TrainingService,  
    public dialogRef: MatDialogRef<SampleListComponent>,
    @Inject(MAT_DIALOG_DATA) public _data, _accountService: AccountService, _uploadService: UploadService) { 
      this.trainingService = _trainingService;
      this.data = _data;
      this.accountService = _accountService;
      this.uploadService = _uploadService;
    }

  ngOnInit() {
  }

  public getSampleList(){
    this.trainingService.getSamples(res => {this.data = res;}, fail => console.log('fail: ', fail))
  }

  uploadImages = (files: string | any[], id: any) => {
    if(files.length == 0){
      return;
    }

    const formData = new FormData();
    for (let index = 0; index < files.length; index++) {
      const element = files[index];
      formData.append('files', element, element.name)
    }

    formData.append('sample_id', id)
    const headers = { 'Authorization': 'Token ' + this.accountService.getCookies('token') };
    this.uploadService.http.post(environment.root_url + "images/image-upload/", formData, {headers}).subscribe(
      res => {
        console.log('res: ', res);
      },
      fail => {
        console.log('fail: ', fail);
      }
    );
  }

  setIsShowAddSample = () => {
    this.showAddSample = !this.showAddSample;
  }


}
