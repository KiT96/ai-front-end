import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Router } from '@angular/router';
import { Shape } from '../models/Shape';
import { ToolService } from '../models/services/tool.service';

@Component({
    selector: 'app-upload-custom',
    templateUrl: './upload-custom.component.html',
    styleUrls: ['./upload-custom.component.css']
})


export class UploadCustomComponent implements OnInit {
    public router: ActivatedRoute;
    public toolService: ToolService = null;
    public data: any;
    public color: string = '';
    imageSrc: string | ArrayBuffer;
    ctx: any;
    widthImg: any;
    heightImg: any;
    canvas: any;
    currentBox: Shape;
    isFirstDown: boolean = false;
    arrPackage: Shape;
    arrBlueText: Shape;
    arrBlackText: Shape;
    orginWidthImg = 0;
    orginHeightImg = 0;

    constructor(private ele: ElementRef, _router: ActivatedRoute, _toolService: ToolService, private navigate: Router) {
        this.router = _router;
        this.toolService = _toolService;
    }

    ngOnInit() {
        this.router.queryParams
            .subscribe(params => {
                this.data = params;
            });
    }

    @ViewChild('imageUpload', { static: false }) imageUpload: ElementRef;
    drawPackage = () => {
        this.color = '#dc3545';
        let img = new Image();
        img.src = this.data.file;
        this.orginHeightImg = img.naturalHeight;
        this.orginWidthImg = img.naturalWidth;
        this.widthImg = this.imageUpload.nativeElement.offsetWidth;
        this.heightImg = this.imageUpload.nativeElement.offsetHeight;
        this.initCanvas();
    }

    @ViewChild('imageUpload', { static: false }) imageNumber: ElementRef;
    drawNumber = () => {
        this.color = '#0756ab';
        if(this.ctx != null && this.canvas != null){
            console.log('xxx 1: ', this.ctx, this.canvas);
            if(this.arrPackage != undefined){
                this.arrPackage.draw(this.ctx)
            }
        }else{
            let img = new Image();
            img.src = this.data.file;
            this.orginHeightImg = img.naturalHeight;
            this.orginWidthImg = img.naturalWidth;
            this.widthImg = this.imageNumber.nativeElement.offsetWidth;
            this.heightImg = this.imageNumber.nativeElement.offsetHeight;
            this.initCanvas();
        }
    }

    @ViewChild('imageUpload', { static: false }) imageCode: ElementRef;
    drawCode = () => {
        this.color = '#000';
        if(this.ctx != null && this.canvas != null){
            console.log('xxx 2: ', this.ctx, this.canvas);
        }else{
            let img = new Image();
            img.src = this.data.file;
            this.orginHeightImg = img.naturalHeight;
            this.orginWidthImg = img.naturalWidth;
            this.widthImg = this.imageCode.nativeElement.offsetWidth;
            this.heightImg = this.imageCode.nativeElement.offsetHeight;
            this.initCanvas();
        }  
    }

    initCanvas() {
        this.canvas = this.ele.nativeElement.querySelector('canvas');
        this.ctx = this.canvas.getContext('2d');
        this.ctx.width = this.widthImg;
        this.ctx.height = this.heightImg;
        this.canvas.width = this.widthImg;
        this.canvas.height = this.heightImg;
    }

    onMousedown(event: { target: { getBoundingClientRect: () => any; }; clientX: number; clientY: number; }) {
        console.log('chạy vào đây: ', this.isFirstDown);
        if (!this.isFirstDown) {
            
            const newBox = new Shape(this.color);
            this.currentBox = newBox;
            const rect = event.target.getBoundingClientRect();
            const pointer = {
                x: event.clientX - rect.left,
                y: event.clientY - rect.top,
            };
            console.log(pointer);
            this.currentBox.x0 = pointer.x;
            this.currentBox.y0 = pointer.y;
            console.log('currentBox: ', this.currentBox);

            this.saveResult(this.currentBox)
            this.isFirstDown = true;
        } else {
            this.currentBox.draw(this.ctx);
            this.isFirstDown = false;
        }
    }

    onMousemove(event: { target: { getBoundingClientRect: () => any; }; clientX: number; clientY: number; }) {
        if (this.isFirstDown) {
            const rect = event.target.getBoundingClientRect();
            const pointer = {
                x: event.clientX - rect.left,
                y: event.clientY - rect.top,
            }
            this.ctx.clearRect(0, 0, this.canvas.width, this.ctx.height);
            this.currentBox.x1 = pointer.x;
            this.currentBox.y1 = pointer.y;
            this.currentBox.draw(this.ctx);
            this.ctx.restore();
        }
    }

    onMouseup(event) {
        if (this.isFirstDown) {
            this.isFirstDown = false;
            if (this.currentBox.isEnable()) {
                this.currentBox.convertX();
                this.currentBox.convertY();
            }
            console.log(this.arrPackage);
            console.log(this.arrBlueText);
            console.log(this.arrBlackText);

            if (this.arrPackage != undefined) {
                this.arrPackage.draw(this.ctx)
            }

            if (this.arrBlueText != undefined) {
                this.arrBlueText.draw(this.ctx)
            }

            if (this.arrBlackText != undefined) {
                this.arrBlackText.draw(this.ctx)
            }
        }
    }

    saveResult = (shape: Shape) => {
        console.log('color: ', shape.borderColor);
        switch (shape.borderColor) {
            case '#dc3545':
                this.arrPackage = shape;
                break;

            case '#0756ab':
                this.arrBlueText = shape;
                break;

            case '#000':
                this.arrBlackText = shape;
                break;
        }
    }

    submit = () => {
        if(this.arrPackage != undefined && this.arrBlueText != undefined && this.arrBlackText != undefined){
            var data = {
                "image_id": this.data.id,
                "package_image": [
                    {
                        "x0": Math.round(this.arrPackage.x0),
                        "y0": Math.round(this.arrPackage.y0),
                        "x1": Math.round(this.arrPackage.x1),
                        "y1": Math.round(this.arrPackage.y1)
                    }
                ],
                "big_text": [
                    {
                        "x0": Math.round(this.arrBlueText.x0),
                        "y0": Math.round(this.arrBlueText.y0),
                        "x1": Math.round(this.arrBlueText.x1),
                        "y1": Math.round(this.arrBlueText.y1)
                    }
                ],
                "small_text": [
                    {
                        "x0": Math.round(this.arrBlackText.x0),
                        "y0": Math.round(this.arrBlackText.y0),
                        "x1": Math.round(this.arrBlackText.x1),
                        "y1": Math.round(this.arrBlackText.y1)
                    }
                ]
            }
            
            this.toolService.addLabel(data, () => {
               this.ctx.clearRect(0,0,this.canvas.width, this.canvas.height)
               alert('Upload success!')
               this.navigate.navigate(['/detect']);
            }, fail => console.log(fail))
        }
        else{
            alert('Vui lòng vẽ cho xong đi nhen')
        }
        
    }
}
