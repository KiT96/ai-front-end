import { Component, OnInit } from '@angular/core';
import { TrainingService } from '../models//services/training.service';

@Component({
  selector: 'app-siamese-list',
  templateUrl: './siamese-list.component.html',
  styleUrls: ['./siamese-list.component.css']
})
export class SiameseListComponent implements OnInit {

  public trainingService: TrainingService = null;
  public data: any;
  public page: number = 1;

  public displayedColumns: string[] = ['id', 'filename', 'created', 'using','action'];

  constructor(_trainingService: TrainingService) { this.trainingService = _trainingService }

  ngOnInit() {
    this.getSiameseList();
  }

  public async getSiameseList(){
    this.trainingService.getResource(res => {this.data = res;
     }, fail => console.log('fail: ', fail))
  }

  uploadImages = (event) => {
    console.log('siamese: ', event);
    
  }
}
