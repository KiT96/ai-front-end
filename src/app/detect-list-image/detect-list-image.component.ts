import { Component, OnInit } from '@angular/core';
import { UploadService } from '../models/services/upload.service';
import { AccountService } from '../models/services/account.service';
import { HttpEventType } from '@angular/common/http';
import { Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { environment } from "../../environments/environment";

function chooseFile() {
  document.getElementById('upload').click();
}

@Component({
  selector: 'app-detect-list-image',
  templateUrl: './detect-list-image.component.html',
  styleUrls: ['./detect-list-image.component.css']
})
export class DetectListImageComponent implements OnInit {
  public datas: any;
  public images = [];
  public imgUrl: string = ''
  public page: number = 1;
  public uploadService: UploadService = null;
  public accountService: AccountService = null;
  public router: Router;

  constructor(_uploadService: UploadService, _accountService: AccountService, _router: Router, private cookieService: CookieService) {
    this.uploadService = _uploadService;
    this.accountService = _accountService;
    this.router = _router;
  }

  ngOnInit() {
    chooseFile()
  }

  initImages = (event: any) => {
    if (event.target.files.length == 0) {
      return;
    }
    for (const key in event.target.files) {
      if (event.target.files.hasOwnProperty(key)) {
        let file = event.target.files[key];
        const reader = new FileReader();
        reader.readAsDataURL(<File>file);
        reader.onload = (_event) => {
          var tmp = { file, url: reader.result.toString(), code: '' }
          this.images.push(tmp)
        }
      }
    }
  }

  changeCode = (event: any) => {
    console.log('event: ', event);
    let code = event.target.value;
    for (const key in this.images) {
      if (this.images.hasOwnProperty(key)) {
        const element = this.images[key];
        element.code = code;
      }
    }
  }

  uploadImages = () => {
    const formData = new FormData();
    
    for (const key in this.images) {
      if (this.images.hasOwnProperty(key)) {
        const element = this.images[key];

        let fileToUpload = <File>element.file;

        formData.append('file', fileToUpload, element.file.name);
        formData.append('code', element.code);
      }
    }

    const headers = { 'Authorization': 'Token ' + this.accountService.getCookies('token') };
    this.uploadService.http.post(environment.root_url + 'detection/yolo/predict/', formData, { headers, reportProgress: true, observe: 'events' })
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress) Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          console.log('Response: ', event);
          this.router.navigate(['/detect']);
        }
      }, fail => { })
  }

}
