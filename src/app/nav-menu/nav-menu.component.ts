import { Component, OnInit, Inject } from '@angular/core';
import { AccountService } from '../models/services/account.service';
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  public accountService: AccountService = null;
  isExpanded = false;
  public baseUrl: string = '';
  public email: string = ''
  
  constructor(@Inject("BASE_URL") _baseUrl: string, _accountService: AccountService, private cookieService: CookieService) { this.baseUrl = _baseUrl, this.accountService = _accountService }

  ngOnInit(): void {
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  logout(){
    this.accountService.logout();
  }

}
