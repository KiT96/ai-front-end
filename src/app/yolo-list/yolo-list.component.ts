import { Component, OnInit } from '@angular/core';
import { TrainingService } from '../models/services/training.service';

@Component({
  selector: 'app-yolo-list',
  templateUrl: './yolo-list.component.html',
  styleUrls: ['./yolo-list.component.css']
})
export class YoloListComponent implements OnInit {

  public trainingService: TrainingService = null;
  public data: any;
  public page: number = 1;

  public displayedColumns: string[] = ['id', 'filename', 'created', 'using','action'];

  constructor(_trainingService: TrainingService) { this.trainingService = _trainingService }

  ngOnInit() {
    this.getYoloList();
  }

  public async getYoloList(){
    this.trainingService.getResource(res => {this.data = res;
     }, fail => console.log('fail: ', fail))
  }

  uploadImages = (event) => {
    console.log('yolo: ', event);
    
  }

}
