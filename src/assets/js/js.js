var temp = 0;
var csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
class Shape {
    constructor(color) {
        temp++;
        this.id = temp;
        this.x0 = this.x0;
        this.y0 = this.y0;
        this.x1 = this.x1;
        this.y1 = this.y1;
        this.originX = null;
        this.originY = null;
        this.borderColor = color;
        this.note = this.note !== '' ? this.note : none;
    }
    isEnable() {
        return this.x0 && this.y0 && this.x1 && this.y1;
    }
    convertX() {
        if (this.x0 > this.x1) {
            const x = this.x0;
            this.x0 = this.x1;
            this.x1 = x;
        }
    }
    convertY() {
        if (this.y0 > this.y1) {
            const y = this.y0;
            this.y0 = this.y1;
            this.y1 = y;
        }
    }
    draw(ctx) {
        ctx.lineWidth = 3;
        ctx.strokeStyle = this.borderColor;
        ctx.strokeRect(this.x0, this.y0, this.x1 - this.x0, this.y1 - this.y0);
    }

    isHover(pointer) {
        if (pointer.x > this.x0 && pointer.x < this.x0 + (this.x1 - this.x0) && pointer.y > this.y0 && pointer.y < this.y0 + (this.y1 - this.y0)) {
            return true;
        }
        return false;
    }
}
document.addEventListener('DOMContentLoaded', function() {
    let inputUpload = document.getElementById('input-file');
    let imgUpload = document.getElementById('upload-image');
    let divImgContain = document.querySelector('.img-contain');
    const canvas = document.getElementById('canvas-shapes');
    let ctx;
    if (canvas) {
        ctx = canvas.getContext('2d');
        ctx.save();
    }
    let orginWidthImg = 0;
    let orginHeightImg = 0;
    let widthImg = 0;
    let heightImg = 0;
    let isFirstDown = false;
    let package = new Shape('#dc3545');
    let blueText = new Shape('#0756ab');
    let blackText = new Shape('#000');
    let arrPackage = [];
    let arrBlueText = [];
    let arrBlackText = [];
    let currentShape;
    let oldShape;
    oldShape = currentShape;
    const btnPackage = document.getElementById('package');
    const btnBlueText = document.getElementById('blue-text');
    const btnBlackText = document.getElementById('black-text');
    const btnSubmit = document.getElementById('shapes-submit');
    const containUpload = document.querySelector('.container-upload');
    const drawContain = document.querySelector('.draw-contain');
    const API_DOMAIN = '';
    const API_SEND_SHAPES = '';
    let isDraw = '';
    let arrNoteContain = [];
    inputUpload.addEventListener('change', function(e) {
        readImage(this);
    });

   function preventFile() {
        const arrFile = ['JPG', 'JPEG', 'PNG', 'GIF', 'BMP', 'TIFF'];
        var fileName = inputUpload.value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toUpperCase();
        if (arrFile.indexOf(extFile) > -1) {
            return true
        }
        return false;

    }

    function readImage(input) {
        const reader = new FileReader();
        reader.onload = function(e) {
            const dataUrl = reader.result;
            imgUpload.src = dataUrl;
            if (preventFile()) {
                imgUpload.onload = function() {
                    containUpload.style.display = 'none';
                    drawContain.classList.remove('contain-hide');
                    getSize();
                }
            } else {
                alert("Only 'JPG', 'JPEG', 'PNG', 'GIF', 'BMP', 'TIFF' files are allowed!");
            }
        };
        reader.readAsDataURL(input.files[0]);
    }
    function getSize() {
        orginWidthImg = imgUpload.naturalWidth;
        orginHeightImg = imgUpload.naturalHeight;
        widthImg = imgUpload.offsetWidth;
        heightImg = imgUpload.offsetHeight;
        initCanvas(widthImg, heightImg);
    }

    function initCanvas(widthImg, heightImg) {
        ctx.width = widthImg;
        ctx.height = heightImg;
        canvas.setAttribute('width', widthImg + 'px');
        canvas.setAttribute('height', heightImg + 'px');
        drawShapes();
    }

    if (divImgContain) {
        function drawShapes() {
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            arrPackage.forEach(p => {
                if (p.isEnable()) {
                    p.draw(ctx);
                }
            });
            arrBlueText.forEach(blue => {
                if (blue.isEnable()) {
                    blue.draw(ctx);
                }
            });
            arrBlackText.forEach(black => {
                if (black.isEnable()) {
                    black.draw(ctx);
                }
            });
            requestAnimationFrame(() => {
                drawShapes();
            })
        }

        let coordinateContain = document.querySelector('.coordinate-list');
        canvas.addEventListener('mousedown', function(e) {
            deleteInputNote();
            if (!isFirstDown) {
                if (isDraw == 'package') {
                    let newShap = new Shape('#dc3545');
                    arrPackage.push(newShap);
                    currentShape = newShap;
                    const rect = canvas.getBoundingClientRect();
                    currentShape.x0 = event.clientX - rect.left;
                    currentShape.y0 = event.clientY - rect.top;
                    isFirstDown = true;
                }
                if (isDraw == 'blue') {
                    let newShap = new Shape('#0756ab');
                    arrBlueText.push(newShap);
                    currentShape = newShap;
                    const rect = canvas.getBoundingClientRect();
                    currentShape.x0 = event.clientX - rect.left;
                    currentShape.y0 = event.clientY - rect.top;
                    isFirstDown = true;
                }
                if (isDraw == 'black') {
                    let newShap = new Shape('#000');
                    arrBlackText.push(newShap);
                    currentShape = newShap;
                    const rect = canvas.getBoundingClientRect();
                    currentShape.x0 = event.clientX - rect.left;
                    currentShape.y0 = event.clientY - rect.top;
                    isFirstDown = true;
                }
            }
        });

        canvas.addEventListener('mousemove', function(e) {
            if (isFirstDown) {
                const rect = canvas.getBoundingClientRect();
                const pointer = {
                    x: event.clientX - rect.left,
                    y: event.clientY - rect.top,
                }
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                currentShape.x1 = pointer.x;
                currentShape.y1 = pointer.y;
                ctx.restore();
            }
        });


        document.addEventListener('mouseup', function(e) {
            if (isFirstDown) {
                if (currentShape.isEnable()) {
                    currentShape.convertX();
                    currentShape.convertY();
                    showCoordiante(currentShape);
                }
                if (currentShape.borderColor !== '#dc3545' && currentShape.isEnable()) {
                    const canvas = e.target;
                    addTextNote(currentShape, canvas);
                }

            }
            isFirstDown = false;
        });

        btnPackage.addEventListener('click', function() {
            isDraw = 'package';
            currentShape = package;
        });

        btnBlueText.addEventListener('click', function() {
            isDraw = 'blue';
            currentShape = blueText;
        });

        btnBlackText.addEventListener('click', function() {
            isDraw = 'black';
            currentShape = blackText;
        });

        function showCoordiante(shape) {
            const li = document.createElement('LI');
            li.classList.add('shape-item');
            const lab = document.createElement('LABEL');
            lab.classList.add('shape-title');
            if (shape.borderColor === '#dc3545') {
                lab.innerHTML = 'Shape package:';
            }
            if (shape.borderColor === '#0756ab') {
                lab.innerHTML = 'Shape blue text:';
            }
            if (shape.borderColor === '#000') {
                lab.innerHTML = 'Shape black text:';
            }
            const div = document.createElement('DIV');
            div.classList.add('coordinate-item');
            const btn = document.createElement('BUTTON');
            btn.classList.add('btn-delete');
            btn.innerHTML = 'Delete';
            btn.setAttribute('data-id', shape.id);
            coordinateContain.appendChild(li);
            const spanX0 = document.createElement('SPAN');
            spanX0.innerHTML = 'X0:' + shape.x0;
            const spanY0 = document.createElement('SPAN');
            spanY0.innerHTML = 'Y0:' + shape.y0;
            const spanX1 = document.createElement('SPAN');
            spanX1.innerHTML = 'X1:' + shape.x1;
            const spanY1 = document.createElement('SPAN');
            spanY1.innerHTML = 'Y1:' + shape.y1;
            div.appendChild(spanX0);
            div.appendChild(spanY0);
            div.appendChild(spanX1);
            div.appendChild(spanY1);
            li.appendChild(lab);
            li.appendChild(div);
            li.appendChild(btn);
            const btnDeletes = document.querySelectorAll('.btn-delete');
            deleteShape(btnDeletes);
        }

        function deleteShape(btnDeletes) {
            btnDeletes.forEach((btn, i) => {
                btn.addEventListener('click', function() {
                    arrPackage = arrPackage.filter(p => {
                        return p.id != btn.getAttribute('data-id');
                    });
                    arrBlueText = arrBlueText.filter(blue => {
                        return blue.id != btn.getAttribute('data-id');
                    });
                    arrBlackText = arrBlackText.filter(black => {
                        return black.id != btn.getAttribute('data-id');
                    });
                    arrNoteContain.forEach((note, i) => {
                        if (note.getAttribute('data-id') === btn.getAttribute('data-id')) {
                            note.remove();
                        }
                    });
                    btn.parentNode.remove();
                });
            })
        }

        function deleteInputNote() {
            if (arrNoteContain.length > 0) {
                arrNoteContain.forEach(div => {
                    const input = div.querySelector('.note-text');
                    if (input) {
                        if (input.value != '') {
                            div.innerHTML = input.value;
                            input.remove();
                        } else {
                            div.remove();
                        }
                    }
                });
            }
        }

        function addTextNote(shape, canvas) {
            const div = document.createElement('DIV');
            div.classList.add('note-contain');
            div.setAttribute('data-id', shape.id);
            arrNoteContain.push(div);
            const input = document.createElement('INPUT');
            input.classList.add('note-text');
            input.setAttribute('autofocus', 'autofocus');
            shape.note = input.value;
            input.addEventListener('change', (e) => {
                shape.note = e.target.value;
            });
            div.appendChild(input);
            div.style.left = ((shape.x1 < shape.x0 && shape.y1 < shape.y0) ? shape.x0 + 5 : shape.x1 + 5) + 'px';
            div.style.top = ((shape.y0 < shape.y1 && shape.x1 > shape.x0) ? shape.y0 : shape.y1) + 'px';
            divImgContain.appendChild(div);
        }

        function realCoordiantes(x1, y1) {
            const x0 = Math.ceil((x1 * orginWidthImg) / widthImg);
            const y0 = Math.ceil((y1 * orginHeightImg) / heightImg);
            return { x0, y0 };
        }

        function returnData(arrData) {
            const dataSend = [];
            arrData.forEach(data => {
                let temp = {};
                data.convertX();
                data.convertY();
                const result0 = realCoordiantes(data.x0, data.y0);
                const result1 = realCoordiantes(data.x1, data.y1);
                temp.x0 = result0.x0;
                temp.y0 = result0.y0;
                temp.x1 = result1.x0;
                temp.y1 = result1.y0;
                temp.note = data.note;
                dataSend.push(temp);
            });
            return dataSend;
        }

        btnSubmit.addEventListener('click', function(e) {
            const dataPackage = returnData(arrPackage);
            const dataBlue = returnData(arrBlueText);
            const dataBlack = returnData(arrBlackText);
            const dataForm = new FormData();
            dataForm.append('file', inputUpload.files[0]);
            dataForm.append('0', JSON.stringify(dataPackage));
            dataForm.append('1', JSON.stringify(dataBlue));
            dataForm.append('2', JSON.stringify(dataBlack));
            dataForm.append('sample', sample_name);
            var xhttp = new XMLHttpRequest();
            var url = API_DOMAIN + API_SEND_SHAPES;
            xhttp.onreadystatechange = function() {
                if (this.readyState === 4 && this.status === 200) {
                    alert('Submit success');
                    window.location.reload();
                }
                if (this.readyState === 4 && this.status !== 200) {
                    alert('Error' + this.status + ': ' + this.statusText);
                }
            }
            xhttp.open('POST', url, true);
            xhttp.setRequestHeader('X-CSRFToken', csrftoken)
            xhttp.send(dataForm);
        });
    }
});